<?php
error_reporting (E_ALL & ~E_NOTICE);

// Header'lari duzenle
ob_clean ();
header ('Content-Type: application/json; charset=UTF-8', TRUE);
header ('Cache-Control: no-cache', TRUE);
header ('Access-Control-Allow-Origin: *', TRUE);

// Kapanista buffer'i yazdir
register_shutdown_function (function () { global $shP; shmop_close ($shP); ob_end_flush (); });

// Komut gelmedi ise hata ver ve cik
if (!isset ($_GET['c']))
  die ('{status:"error", message:"No command specified!"}');

// InMem isleri
$shP = @shmop_open (0xabcd, 'c', 0600, 10240) or die ('{status:"error", message:"In memory DB error!"}');

function TasksGet ()
{
  global $shP;

  $data = shmop_read ($shP, 0, 0);	// tum hafizayi getir (pozisyon bilgisi tutulmadigi icin)
  $term = strpos ($data, 0x00);		// null terminated geldigi icin, gercek verinin bittigi yeri bul
  return (!$term) ? [] : unserialize (substr ($data, 0, $term));	// veriyi array olarak dondur
}

function TaskAdd ($task)
{
  global $shP;

  $data = array_merge (TasksGet (), [$task]);	// Tum verinin sonuna yeni gorevi ekle
  shmop_write ($shP, serialize ($data), 0);	// Tum veriyi yazdir

  return count ($data);	// Yeni veri sayisini dondur
}

// Motor
switch ($_GET['c'])
{
  case 'get':
    $tasks = TasksGet ();
    echo json_encode (['status' => 'ok', 'message' => count ($tasks) . ' tasks found.', 'tasks' => $tasks]);
    break;

  case 'set':
    if (!isset ($_GET['task']) || empty ($_GET['task']))
      die ('{status:"error", message:"Empty task to record!"}');

    $total = TaskAdd (base64_decode ($_GET['task']));
    echo json_encode (['status' => 'ok', 'message' => 'New task recorded at position ' . $total]);
    break;
}
?>
